import React, { Component } from 'react'
import { Link } from 'react-router-dom'
//获取方法(设置的时候需要引入，获取的时候直接获取即可)
import { Setuser } from '../store/action'
import hjm100 from '../assets/hjm100.png'
import hjm from '../assets/hjm.png'
// 使用外部css文件表
import '../styles/views/Home.css'
import Qrcode from '../components/Qrcode'
import ImgShear from '../components/ImgShear'
import Countdown from '../components/Countdown'
import Search from '../components/Search'
class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {},
      searchDataList: []
    }
  }
  // 该方法在首次渲染之前调用
  componentWillMount() {
    //初始化信息
    this.getStoreUser()
  }

  getImgUrl = imgurl => {
    //再此处执行一个修改图片的ajax请求
    console.log(imgurl)
  }
  //通过js跳转路由
  goAddress = () => {
    this.props.history.push({
      pathname: '/set/edit/address'
    })
  }
  setStoreUser = () => {
    let nameVal = this.refs.name_input.value
    let phoneVal = this.refs.phone_input.value
    React.store.dispatch(Setuser('name', nameVal))
    React.store.dispatch(Setuser('phone', phoneVal))
    this.getStoreUser()
  }
  getStoreUser = () => {
    this.setState({
      user: React.store.getState().user
    })
  }
  // 该方法会创建一个虚拟DOM，用来表示组件的输出。
  // 搜索组件
  searchData = value => {
    // 调用搜索接口传递数据参数
    let searchDataList = [
      { id: 1, name: '大米1' },
      { id: 2, name: '大米21' },
      { id: 3, name: '大米32' },
      { id: 4, name: '大米43' },
      { id: 4, name: '小米43' }
    ]
    // 模拟模糊查询
    const reg = new RegExp(value)
    this.setState({
      searchDataList: searchDataList.filter(item => reg.test(item.name))
    })
  }
  getListDataItem = item => {
    console.log(item)
    let searchDataList = [
      { id: 1, name: '大米1' },
      { id: 2, name: '大米21' },
      { id: 3, name: '大米32' },
      { id: 4, name: '大米43' },
      { id: 4, name: '小米43' }
    ]
    const data = searchDataList.filter(searchData => searchData.id === item.id)
    console.log(data)
  }
  render() {
    return (
      <div id="Home">
        <div className="Home-search__box">
          <Search
            getListDataItem={this.getListDataItem}
            dataList={this.state.searchDataList}
            onChange={this.searchData}
            placeholder="请输入项目名称，设备编号"
          ></Search>
        </div>
        <Link className="go_btn go_location" to={`/set/edit/address/张三`}>
          前往定位
        </Link>
        <header className="Home-header">
          <div className="home-img flex-center">
            <ImgShear
              defImg={hjm}
              imgWidth={200}
              imgHeight={200}
              getImgUrl={this.getImgUrl.bind(this)}
            />
          </div>
          <h1 className="Home-title">Welcome to React</h1>
        </header>
        <div className="Home-qrbox flex-align">
          <div className="qr-text">
            <Qrcode qrUrl={'https://hjm100.cn'} qrText={'HJM100'} />
          </div>
          <div className="qr-img">
            <Qrcode
              qrUrl={'https://www.npmjs.com/package/hjm-web-staging'}
              qrLogo={hjm100}
              qrLogoSize={48}
            />
          </div>
        </div>
        <div className="storeDemo">
          <div className="inputBox flex-center">
            <input
              ref="name_input"
              type="text"
              name="name"
              placeholder="请输入姓名"
            />
            <input
              ref="phone_input"
              type="text"
              name="phone"
              placeholder="请输入手机号"
            />
            <button onClick={this.setStoreUser}>set store</button>
          </div>
          <Countdown
            className="Error-time"
            startTime={1531891577}
            endTime={1531891587}
            msg="倒计时结束了"
          ></Countdown>
          <p>开发者名字：{this.state.user.name}</p>
          <p>开发者电话：{this.state.user.phone}</p>
          <Link
            className="go_btn go_bagWheel"
            to={{ pathname: '/activity/bagWheel', query: { name: 'hjm100' } }}
          >
            大转盘
          </Link>
        </div>
      </div>
    )
  }
}

export default Home
