import React, { Component } from 'react'
import '../styles/components/SetMapMarker.css'
const AMap = window.AMap
/***************************************
  由于Chrome、IOS10等已不再支持非安全域的浏览器定位请求，为保证定位成功率和精度，请尽快升级您的站点到HTTPS。
***************************************/
class SetMapMarker extends Component {
  constructor(props) {
    /** 父组件传递参数
     * 搜索组件
     * @argument markerPositionList     位置列表数据
     * @argument isLine                 是否显示线
     * @argument isLoop                 线路是否闭环(默认不必换)
     */
    super(props)
    this.state = {
      map: null
    }
  }
  static defaultProps = {
    markerPositionList: [{
      address: '北京',
      isLable: true,
      icon: "https://webapi.amap.com/theme/v1.3/markers/n/mark_r.png",
      position: [116.405467, 39.907761]
    }],
    isLine: false,
    isLoop: false
  }
  componentWillMount() {}
  componentDidMount = () => {
    this.setState({
      //加载地图
      map: new AMap.Map('mapMarker')
    },() =>{
      this.SetMapMarker()
    })
  }
  // 设置点覆盖物
  SetMapMarker(){
    // 根据父组件传递参数处理逻辑
    const { markerPositionList, isLine, isLoop } = this.props
    markerPositionList.forEach(async (item)=>{
      let marker = new AMap.Marker(item);
      if (item.isLable) {
        marker.setLabel({
          offset: new AMap.Pixel(20, 20),  //设置文本标注偏移量
          content: `
            <p style='max-width:80px;text-overflow: ellipsis;overflow: hidden;' class='marker-lable'>
              ${item.address || await this.getAddress(item.position)}
            </p>`, //设置文本标注内容
          direction: 'right' //设置文本标注方位
        });
      }
      this.state.map.add(marker);
    })
    // 参数参考：https://lbs.amap.com/api/javascript-api/reference/overlay#polyline
    if (isLine) {
      // 坐标数组(在划线的时候用)
      let positionArr = markerPositionList.map(item=>(item.position))
      if (isLoop) {
        // 创建闭环
        positionArr.push(positionArr[0])
      }
      let polyline = new AMap.Polyline({
        path: positionArr,
        strokeColor: "#ff0000",       // 线条颜色
        strokeWeight: 2,              // 线条宽度
        strokeStyle: "dashed",        // 线条样式 实线:solid，虚线:dashed
        strokeDasharray: [10, 5],     // 勾勒形状轮廓的虚线和间隙的样式
        lineJoin: 'round',            // 折线拐点的绘制样式，默认值为'miter'尖角，其他可选值：'round'圆角、'bevel'斜角
        lineCap: 'round',             // 折线两端线帽的绘制样式，默认值为'butt'无头，其他可选值：'round'圆头、'square'方头
      })
      polyline.setMap(this.state.map)
    }
    // 缩放地图到合适的视野级别
    //自动适配到指定视野范围
    // map.setFitView([marker1, marker2]);
    // 无参数时，自动自适应所有覆盖物
    // map.setFitView();
    this.state.map.setFitView()
  }
  // 根据坐标获取地址
  getAddress=(lnglat)=>{
    return new Promise((resolve,reject)=>{
      this.state.map.plugin('AMap.Geocoder', function() {
        let geocoder = new AMap.Geocoder()
        geocoder.getAddress(lnglat, function(status, result) {
          if (status === 'complete' && result.info === 'OK' && result.regeocode) {
            // result为对应的地理位置详细信息
            resolve(result.regeocode.formattedAddress)
          }
        })
      })
    })
  }
  render() {
    return (
      <div id="SetMapMarker">
        <div id="mapMarker" className="map"></div>
      </div>
    )
  }
}

export default SetMapMarker
