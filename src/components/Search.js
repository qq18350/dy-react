import React, { Component } from 'react'
import '../styles/components/Search.css'
import searchIcon from '../assets/search-icon.png'
class Search extends Component {
  constructor(props) {
    /** 父组件传递参数
     * 搜索组件
     * @argument placeholder        输入框占位文本
     * @argument dataList           列表数据
     * @argument dataKey            列表数据显示key值
     * @function onChange()            触发搜索事件
     * @function getListDataItem()  获取单条列表项
     */
    super(props)
    this.state = {
      value: '',
      display: { display: 'none' }
    }
  }
  static defaultProps = {
    dataList: [],
    dataKey: 'name',
    placeholder: '请输入项目名称，设备编号'
  }
  // 每当父组件重新render导致的重传props，子组件将直接跟着重新渲染，无论props是否有变化。
  // shouldComponentUpdate = nextProps => {
  //   console.log(nextProps)
  // }
  // 父组件重传props时就会调用这个方法
  componentWillReceiveProps = nextProps => {
    if (nextProps.dataList.length > 0) {
      this.setState({
        display: { display: 'block' }
      })
    }
  }
  componentDidMount = () => {}
  onChange = e => {
    this.setState({
        value: e.target.value
      },() => {
        this.props.onChange(this.state.value)
      }
    )
  }
  getListDataItem = data => {
    this.props.getListDataItem(data)
    // 抛出数据后隐藏列表(看你的需求)
    this.setState({
      value: data[this.props.dataKey],
      display: { display: 'none' }
    })
  }
  render() {
    const { placeholder, dataList, dataKey } = this.props
    return (
      <div className="Search">
        <div className="Search-input">
          <input
            value={this.state.value}
            placeholder={placeholder}
            onChange={this.onChange}
          />
          <img src={searchIcon} alt="搜索" />
        </div>
        <ul className="Search-list" style={this.state.display}>
          {dataList.map((item, index) => (
            <li key={index} onClick={this.getListDataItem.bind(this, item)}>
              {item[dataKey]}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default Search
